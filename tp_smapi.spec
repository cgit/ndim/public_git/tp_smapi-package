#
# References:
# http://fedoraproject.org/wiki/Packaging/SysVInitScript

Name:		tp_smapi
Version:	0.40
Release:	1%{?dist}
Summary:	ThinkPad System Management API - scripts and user interface

Group:		System Environment/Kernel
License:	GPLv2+
URL:		http://tpctl.sourceforge.net/
Source0:	http://prdownloads.sourceforge.net/tpctl/tp_smapi-%{version}.tgz
Source11:	tp_smapi.init
Source12:	tp_smapi.sysconfig
Source21:	tp_smapi.udev-rules

BuildRoot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildArch:	noarch

# The kmod magic in tp_smapi-kmod requires this:
Provides:	tp_smapi-kmod-common = %{version}

Requires:	tp_smapi-kmod >= %{version}
Requires(post):	tp_smapi-kmod >= %{version}


%define udevfname 66-tp_smapi.rules
%define udevrulesdir %{_sysconfdir}/udev/rules.d


%description
ThinkPad System Management API - scripts and user interface


%prep
%setup -q -n tp_smapi-%{version}


%build


%install
%{__rm} -rf "%{buildroot}"

%{__install} -d -m 0755 "%{buildroot}/%{_sysconfdir}/init.d"
%{__install} -p -m 0755 "%{SOURCE11}" "%{buildroot}/%{_sysconfdir}/init.d/tp_smapi"

%{__install} -d -m 0755 "%{buildroot}/%{_sysconfdir}/sysconfig"
%{__install} -p -m 0644 "%{SOURCE12}" "%{buildroot}/%{_sysconfdir}/sysconfig/tp_smapi"

%{__install} -d -m 0755 "%{buildroot}/%{udevrulesdir}"
%{__install} -p -m 0644 "%{SOURCE21}" "%{buildroot}/%{udevrulesdir}/%{udevfname}"


%clean
%{__rm} -rf "%{buildroot}"


%post
chkconfig --add tp_smapi
service tp_smapi condrestart


%preun
service tp_smapi stop
chkconfig --del tp_smapi


%files
%defattr (-,root,root,-)
%doc README CHANGES TODO
%config(noreplace) %{udevrulesdir}/%{udevfname}
%config(noreplace) %{_sysconfdir}/sysconfig/tp_smapi
%{_sysconfdir}/init.d/tp_smapi


%changelog
* Tue Feb 17 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.40-1
- update to tp_smapi-0.40
- run service condrestart instead of start

* Wed Oct 08 2008 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.39-1
- update to tp_smapi-0.39

* Sun Apr 13 2008 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.37-1
- update to tp_smapi-0.37
- add sysvinit script and sysconfig file
- rename package from tp_smapi-common to tp_smapi

* Sun Apr 13 2008 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.34-7
- separate tp_smapi-common package to complement tp_smapi-akmod
